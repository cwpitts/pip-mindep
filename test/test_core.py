from pathlib import Path
from typing import Final

import pytest

# isort: off
from pip_mindep.core import compute_minimum_dependencies

# isort: on

TEST_DIR: Final[Path] = Path(__file__).parent


@pytest.mark.core
def test_compute_minimum_dependencies():
    """Minimum dependency computation should work correctly."""
    expected: Final[list[str]] = [
        "networkx",
        "toml",
        "tox",
    ]
    actual: Final[list[str]] = compute_minimum_dependencies(
        TEST_DIR.parent / "pyproject.toml"
    )

    assert expected == actual
