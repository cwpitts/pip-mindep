# pip-mindep: minimum dependency list
`pip-mindep` computes the *minimum* set of dependencies for a Python
project. It does so in the following way:

1. Compute the dependency graph for the project.
2. For each component in the graph:
   1. Add all nodes in the component with an in-degree of 0 to the
      minimum set.

This will produce the minimum set of dependencies because each node
with an in-degree of 0 cannot be reached from another node. All other
nodes by definition will be reachable from at least one node with an
in-degree of 0.

# Usage
```console
$ pip-mindep -h
usage: pip-mindep [-h] file_path

positional arguments:
  file_path   path to specification file

options:
  -h, --help  show this help message and exit
$ 
```

# Supported file types
Currently, `pip-mindep` supports:

- `setup.py`
- `requirements.txt`
- `pyproject.toml`

# Example
`pip-mindep` running on itself. The explicit dependencies are:

- `networkx`
- `toml`
- `tox`
- `pytest`
- `pytest-cov`
- `pytest-mock`

The output of `pip-mindep pyproject.toml`:

```console
$ pip-mindep pyproject.toml
networkx
toml
tox
$ 
```

Examining the dependencies for `tox` shows that it includes `pytest`,
`pytest-cov`, and `pytest-mock` as transitive dependencies, so they
are not in the minimum set.
