from distutils.core import run_setup
from importlib.metadata import requires
from pathlib import Path
from queue import Queue
from typing import Final, Optional, Union

import networkx as nx
import toml
from networkx import DiGraph
from packaging.requirements import Requirement
from setuptools.dist import Distribution


def compute_graph(file_path: Union[Path, str]) -> DiGraph:
    """Compute dependency graph from file.

    Parameters
    ----------
    file_path : Path
        Path to file

    Returns
    -------
    G : DiGraph
        Dependency graph representing the file

    Notes
    -----
    Currently supported requirements files are requirements.txt and setup.py
    """
    if not isinstance(file_path, Path):
        file_path = Path(file_path)

    dependency_list: Queue = Queue()

    match file_path.name:
        case "setup.py":
            dist: Distribution = run_setup(file_path, stop_after="init")
            for dependency in dist.install_requires:
                dependency_list.put(dependency)
        case "requirements.txt":
            with open(file_path, "r") as in_file:
                for line in in_file:
                    dependency_list.put(line)
        case "pyproject.toml":
            with open(file_path, "r") as in_file:
                config: dict = toml.load(in_file)
            for dependency in config["project"]["dependencies"]:
                dependency_list.put(dependency)
        case other:
            raise ValueError(f"Unsupported requirements file type: {file_path.name}")

    G: DiGraph = DiGraph()

    while not dependency_list.empty():
        req: Requirement = Requirement(dependency_list.get())
        if req.name not in G:
            G.add_node(req.name)
            t_dep_list: Optional[list[str]] = requires(req.name)
            if t_dep_list is None:
                continue
            for t_dep in t_dep_list:
                t_req: Requirment = Requirement(t_dep)
                dependency_list.put(t_req.name)
                G.add_edge(req.name, t_req.name)

    return G


def compute_minimum_dependencies(file_path: Path) -> list[str]:
    """Compute minimum dependencies.

    Parameters
    ----------
    file_path : Path
        Path to file

    Returns
    -------
    dependency_list : list[str]
        Sorted list of the minimum dependencies for the requirements file
    """
    # Compute dependency graph
    G: DiGraph = compute_graph(file_path)

    dependency_list: list[str] = []

    # For each component, take all nodes with in-degree 0 and add them
    # to the dependency list
    components: list[DiGraph] = [
        G.subgraph(c).copy()
        for c in nx.algorithms.components.weakly_connected_components(G)
    ]
    for H in components:
        for node in H.nodes:
            if H.in_degree(node) == 0:
                dependency_list.append(node)

    dependency_list.sort()
    return dependency_list
