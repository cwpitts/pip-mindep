"""pip-mindep.


"""
from typing import Final

from .core import compute_minimum_dependencies

major: Final[int] = 0
minor: Final[int] = 0
patch: Final[int] = 1
__version__: Final[str] = f"{major}.{minor}.{patch}"

__all__: Final[list[str]] = [
    "compute_minimum_dependencies",
]
