"""
"""
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Optional

# isort: off
from pip_mindep.core import compute_minimum_dependencies
# isort: on


def parse_args(argv: Optional[list[str]] = None) -> Namespace:
    """Parse program arguments.

    Parameters
    ----------
    argv : Optional[list[str]]
        Program argument vector

    Returns
    -------
    args : Namespace
        Program arguments
    """
    argp: ArgumentParser = ArgumentParser()
    argp.add_argument("file_path", type=Path, help="path to specification file")

    args: Namespace = argp.parse_args(argv)

    return args


def main(args: Optional[Namespace] = None) -> None:
    """Execute main program logic.

    Parameters
    ----------
    args : Optional[Namespace]
        Program arguments
    """
    if args is None:
        args = parse_args()

    minimum_dependencies: list[str] = compute_minimum_dependencies(args.file_path)

    for item in minimum_dependencies:
        print(item)
